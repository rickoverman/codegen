# coding=utf8
"""
## Code Generator Web UI

## Webbased administrator using the gen.py functionalities.
## Created by Rick Overman
## Copyright (c) 2015 Webvanced.nl

## --------------------------------------------------------------------------
## __init__.py - flask app containing routes & functionalities
## --------------------------------------------------------------------------
"""



# 3rd party modules
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash

# create our little application :)
app = Flask(__name__)


import os, sys
# Our libs path is outside our web projects scope so we include it like this
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
if not path in sys.path:
    sys.path.insert(1, path)
del path

# Now we can use out custom modules
from libs.models import *
import libs.deamon as deam

from clint.textui import prompt, validators, colored


# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'web.db'),
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)



###########################################################################
# Routes 
###########################################################################

@app.route('/')
def dashboard():
 
    if not session.get('logged_in'):
        return redirect(url_for('login')) 
    
    user_session = session.get('logged_in')
    user = ScriptUser.get(ScriptUser.id == user_session['id'])
    client = user.clients[0]
 
    return render_template('list-vhosts.html', vhosts=client.vhosts) 
 

###########################################################################
# Login & Logout
###########################################################################

@app.route('/login', methods=['GET', 'POST'])
def login():

    session['logged_in'] = False
    error = None
 
    if request.method == 'POST':
        try:
            user = ScriptUser.get(ScriptUser.username == request.form['username'], \
            ScriptUser.password == request.form['password'])
            session['logged_in'] = {'id':user.id, 'name':user.name, 'username':user.username}
            flash('You were logged in')
            return redirect(url_for('dashboard')) 
        except:
            error = 'Invalid user!'
 
    return render_template('login.html', error=error)
    

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('login'))


###########################################################################
# Deamon
###########################################################################

 
@app.route('/deamon', methods=['GET','POST'])
def deamon():
    if not session.get('logged_in'):
        abort(401)
    
    # Get the command
    commands = Deamon_command.select()
    client = Client.get(id=1)
    
    # Render the template
    return render_template('deamon.html', client=client, commands=commands)



###########################################################################
# Logs
###########################################################################

@app.route('/list-log', methods=['GET','POST'])
def list_log():
    if not session.get('logged_in'):
        abort(401)
 
    # Render the template
    return render_template('list-log.html')



###########################################################################
# Users
###########################################################################

@app.route('/list-users', methods=['GET'])
def list_users():
    if not session.get('logged_in'):
        abort(401)
       
    users = ScriptUser.select()    
    return render_template('list-users.html', users=users)
     
     
     
###########################################################################
# Clients 
###########################################################################

@app.route('/list-clients', methods=['GET'])
def list_clients():
    if not session.get('logged_in'):
        abort(401)

    clients = Client.select()    
    return render_template('list-clients.html', clients=clients)


@app.route('/user-list-clients')
@app.route('/user-list-clients/<int:userid>', methods=['GET'])
def user_list_clients(userid):
    if not session.get('logged_in'):
        abort(401)
    
    user = ScriptUser.get(ScriptUser.id == userid)
    return render_template('list-clients.html', clients=user.clients)



###########################################################################
# Vhosts
###########################################################################

# List All Vhosts.
@app.route('/list-vhosts', methods=['GET'])
def list_vhosts():
    if not session.get('logged_in'):
        abort(401)
 
    vhosts = Vhost.select()
    return render_template('list-vhosts.html', vhosts=vhosts) 


# List Client Vhosts..
@app.route('/client-list-vhosts')
@app.route('/client-list-vhosts/<int:clientid>', methods=['GET'])
def client_list_vhosts(clientid):
    if not session.get('logged_in'):
        abort(401)
 
    client = Client.get(Client.id == clientid)
    return render_template('list-vhosts.html', client=client)


# List User Vhosts.
@app.route('/user-list-vhosts')
@app.route('/user-list-vhosts/<userid>', methods=['GET'])
def user_list_vhosts(userid):
    if not session.get('logged_in'):
        abort(401)
 
    user = ScriptUser.get(ScriptUser.id == userid)
    client = Client.get(Client.scriptuser == user)
    vhosts = client.vhosts
    return render_template('list-vhosts.html', client=client, user=user)


# Vhost Detail (not in use yet!)
@app.route('/detail-vhost/')
@app.route('/detail-vhost/<vhostid>', methods=['GET'])
def detail_vhost():
    if not session.get('logged_in'):
        abort(401)
    
    vhost = Vhost.get(Vhost.id == vhostid)
    return render_template('detail-vhost.html', vhost=vhost)



###########################################################################
# Settings
###########################################################################

# Settings.
@app.route('/settings', methods=['GET','POST'])
def settings():
    if not session.get('logged_in'):
        abort(401)
    
    return render_template('settings.html') 

 
# More Info.
@app.route('/info', methods=['GET','POST'])
def info():
    return render_template('info.html') 



###########################################################################
# Update acceptation from webUI. 
###########################################################################


# Start update op acceptation (ajax).
@app.route('/update_acceptation', methods=['GET'])
def update_acceptation():
        
    user_session = session.get('logged_in')
    user = ScriptUser.get(ScriptUser.id == user_session['id'])
    client = user.clients[0]
    
    # Send the deamon command.
    command_id = deam.send_command(client,'update',"Send from WebUI")
    
    return str(command_id)


# Check if a specific command is running (ajax).
@app.route('/get_command_status/<int:command_id>', methods=['GET'])
def get_command_status(command_id):
  
    command = Deamon_command.get(Deamon_command.id == command_id)
    
    if command.finished:
        return 'finished'
    else:
        return 'running'
 

# Start the application.
if __name__ == "__main__":
    app.run(debug=True)
    
        


