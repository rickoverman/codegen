function Gon(){
    this.acceptation_update_status;
}

Gon.prototype.initialize = function(){
    this.initialized = true;
}

Gon.prototype.progress_bar_html = function(){

    var html = '<div class="progress">\
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">\
                <span class="sr-only"></span>\
                </div>\
                </div>';
                  
    return html;

}

Gon.prototype.update_acceptation = function(){

    self = this;
    $.get('/update_acceptation', function(command_id){
            //console.log('command id: '+command_id);
            Gon.acceptation_update_status = setInterval(function () {
 
                 $.get('/get_command_status/'+command_id, function(status){
                    //console.log(status);
                    if(status=="finished"){
                        $('#command_status').html('Finished!');
                        $('.bootbox-progress').html('<br>The update of the acceptation enviroment is finished!');
                        $('.progress').html('<br>The update of the acceptation enviroment is finished!');
                        clearInterval(Gon.acceptation_update_status);
                        //self.bootbox.hideAll()
                    }
                    if(status=="running"){
                        $('#command_status').html('Running!');
                    }
                 });
                 
            }, 3000);
            bootbox.alert("<b>Updating Acceptation</b><p class=\"bootbox-progress\"><br>Please wait ..."+self.progress_bar_html()+"</p>", function() {});
    });

}

var gon;
$(function(){

    gon = new Gon();
    gon.initialize();
    
})


