# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

import sys
import os
#sys.path.insert(0, os.path.abspath('..'))

import yaml
import peewee
from clint.arguments import Args
from clint.textui import puts, colored, indent
from jinja2 import Environment, PackageLoader,  Template

import libs.config as config

#print generator.is_dir(args[0])
this_filepath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
settings = config.load(this_filepath+'/bin/settings.yaml')


# Setup an environment to load the templates from
env = Environment(loader=PackageLoader('templates','../templates'))
root_gen = os.path.dirname(os.path.realpath(__file__))




def load(path):
    stream = open(path, 'r')
    return yaml.load(stream)



def files(path):
    filesn = []
    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:

            if name != "codegen.yaml":

                filesn.append(os.path.join(root, name))
    return filesn



def generate(files,keywords = []):
    """Generates the template file"""
    for file in files:
        template = Template(file)
        output_file = template.render(keywords)

        genfile = open(yaml.output_root + output_file, 'w')
        genfile.write(env.get_template(template_file).render(yml=keywords))
        genfile.close()



def render(file, keywords = []):

     return env.get_template(file).render(keywords)

def mkdir():
    try:
        os.makedirs(output_root + os.path.split(output_file)[0])
    except:
        pass


def is_yaml_file(file):
    try:
        return load(file)
    except:
        pass

def is_file(file):

    return os.path.exists(file)


def is_dir(file):
    return os.path.isdir(file)
