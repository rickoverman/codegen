from clint.textui import puts, colored, indent
import os
rows, cols = os.popen('stty size', 'r').read().split()

def get_file_header(text):
    a = ""
    for x in range(0, int(cols)):
        a = a + "="

    out = colored.yellow(a) + "\n "
    out = out + colored.yellow(text,bold=True) + "\n"
    out = out + colored.yellow(a) + ""
    return out


def get_app_header(text):
    a = ""
    for x in range(0, int(cols)):
        a = a + "*"

    out = "\n"
    out = out + colored.green(text,bold=True) #+ "\n"
    #out = out + colored.cyan(text) + "\n"
    # out = out + colored.green(a) + ""
    return out


def get_app_header_text(text):
    out = colored.cyan(text) + "\n"

    return out
