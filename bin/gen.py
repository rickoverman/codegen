#!/usr/bin/env python
# coding=utf8
"""
## Code Template Generator

## Simple command-line utility for generating code templates.
## Check the README.md file for a quick introduction and tutorial.
## Created by Rick Overman
## Copyright (c) 2015 Webvanced.nl

## Run the following commands before you start:

## cd /var/www/dev-tools/codegen
## source venv/bin/activatekobalt
## alias gen="python /var/www/dev-tools/codegen/bin/gen.py"

## --------------------------------------------------------------------------
## gen.py - Code template generation utility (gen)
## --------------------------------------------------------------------------
"""

import os, sys
# Our libs path is outside our projects scope so we include it like this
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if not path in sys.path:
    sys.path.insert(1, path)
del path


import peewee
import collections
from clint.arguments import Args
from clint.textui import puts, colored, indent
from jinja2 import Environment, PackageLoader,  Template


import libs.config as config
import libs.common as common
import libs.generator as generator


args = Args()

if not args[0]:
    print colored.red('No template folder, file or config.yaml file selected!')
    sys.exit()



#print generator.is_dir(args[0])
this_filepath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
settings = config.load(this_filepath+'/bin/settings.yaml')


os.system('printf "\033c"')

db = peewee.MySQLDatabase("kolfbonddb",host="localhost",port=3306,user="root",passwd="root")
db.connect()


print common.get_app_header(" Code template generator cli")
print common.get_app_header_text(" (c) 2015 Webvanced.nl")



if generator.is_yaml_file(args[0]):
    """Generate from config.yaml file"""
    # print 1
    yaml_data = config.load(args[0])
    yaml_data = generator.load(folder_yaml_file)
    yaml_data['colored'] = colored
    yaml_data['db'] = db

    for template in generator.files(settings['template_root']):

        if args[1] == "-w":
            generator.generate(template, yaml_data)
        else:
            print generator.render(template, yaml_data)


elif generator.is_dir(args[0]):
    """Print to console if the file and valid config.yaml is found in the folder root"""
    # print 3
    folder_yaml_file = os.path.dirname(args[0])+"/"+settings['yaml_file']
    # print folder_yaml_file
    if generator.is_yaml_file(folder_yaml_file):
        yaml_data = generator.load(folder_yaml_file)


        yaml_data['colored'] = colored
        yaml_data['db'] = db

        values = yaml_data[yaml_data['generator']['loop_tag']]

        # print 2
        for val in values:


            yaml_data[yaml_data['generator']['loop_tag']] = val



            for template in generator.files(args[0]):

                print common.get_file_header(template +" - " + val)

                if args[1] == "-w":
                    generator.generate(template, yaml_data)
                else:
                    print generator.render(template, yaml_data)



elif generator.is_file(args[0]):
    """Print to console if the file and valid config.yaml is found in the folder root"""
    # print 3
    folder_yaml_file = os.path.dirname(args[0])+"/"+settings['yaml_file']

    # print folder_yaml_file
    #sys.exit()
    if generator.is_yaml_file(folder_yaml_file):
        yaml_data = generator.load(folder_yaml_file)
        yaml_data['colored'] = colored
        yaml_data['db'] = db


        print generator.render(args[0], yaml_data)


else:

    print colored.red("Not found!: " + args[0])





sys.exit()


# output configurations
print colored.yellow('Title: ' + config['generator']['title'])
print colored.yellow('Author: ' + config['generator']['author'])
print colored.yellow('Description: ' + config['generator']['description'])
print colored.yellow('Output path: ' + output_root)

#collections.namedtuple('Person', 'name class age gender')
