# Code Generation Utility

Please note this document is subject to change.

This document describes the usage of the template generator `gen.py`

`gen.py` is a python commandline script for generating templated code from template projects located in the `templates` folder.

The `templates` folder holds diffrent project subfolders like `wasengine` that holds the templates needed to generate code specific to the wasengine framework. Every project folder should be supported by a `codegen.yaml` and `README.md` file.

The `codegen.yaml` file values are passed to the code templates and folder & filenames

keywords:

    {{ tablename }}
    {{ keyword2 }}
    {{ keyword2.val1 }}
    etc...

The template has special `db` helper for retrieving database tables and columns.

    {{ db.get_tables() }}
    {{ db.get_columns(table) }}


Its also posible to generate and print directly to the console from single code template files and use the helper `colored` to uitput text in color.

    {{ colored.yellow("This is yellow text") }}
    {{ colored.blue("This is blue text in bold",bold=True) }}


The generated output will have the same folder structure as is defined in the templates project folder but with the generated filenames, foldernames and templates.


Requirements
------------

### Python & Virtualenv

Virtualenv is used to manage python and package requirements. You need to run the script with the virtual enviroment activated located in `venv/`.

To activate virtualenv

    source venv/bin/activate

The following packages are installed under virtualenv

    pip install butterfly
    pip install pymysql
    pip install pyyaml
    pip install clint
    pip install flask
    pip install peewee


### Codegen folder-structure

```
gen
  |- webui                 # Flask GUI
  |- libs                  # Shared libraries for Flask & codegen.py
  |- workspaces            # Workspaces vault
  |   |- default           # The default workspace
  |       |- wasengine     # Basic wasengine generator
  |       |- yii           # Basic yii generator
  |       |- examples      # Example files
  |- gen.py                # Commanline utility

```


### GUI (Flask)

The GUI for the `gen.py` command is created using flask ([http           ://flask.pocoo.org/](http://flask.pocoo.org/))

To start the webserver use:

    python gui/start.py

the gui is located at designated location, usually:

    http://localhost:5000/



### Command-line Example usage:

The codegenerator needs a `codegen.yaml` config file to be located in de project root.

Generate all files recursive in projectfolder:

    gen projectfolder/codegen.yaml

When no yaml file is specified de generator checks automaticly if the file codegen.yaml is available in the project root, so the following command is the same as above.

    gen projectfolder/


You can also generate a single file within a project and print it to the console for easy copy pasting.

    gen examples/db




The above generates the templates located in the `templates/wasengine/object` folder and creates the same file&folder structure in the output location root as defined in the `config.yml` located in the template project root , in this case `templates/wasengine/config.yml`

The generator replaces all keyword1 keyword2 etc. located in the templates,file and foldernames and replace it with our defined values seperated by spaces. The amount of keywords accepted defaults to 10 but can be configured in de `config.yml` file in the template folder root.

### Common commands
