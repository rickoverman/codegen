<?php

/**
 * @version {{ generator.version }}
 * @package {{ tablename }}
 * @author {{ generator.author }}
 * @copyright Copyright (c) 2015, webvanced.nl
 */


$image_field_suffix = "";

//resize the original image1 when there to big
$WASengine->config['resize_image']['image1'.$image_field_suffix]['maxwidth'] = 1024;        				//max width in pixels
$WASengine->config['resize_image']['image1'.$image_field_suffix]['maxheight'] = 1024;        				//max height in pixels
$WASengine->config['resize_image']['image1'.$image_field_suffix]['resizemethod'] = "keep_aspect";
$WASengine->config['resize_image']['image1'.$image_field_suffix]['croptosize'] = false;
$WASengine->config['resize_image']['image1'.$image_field_suffix]['cropmethod'] = "cropmethod";				//center, top, bottom, left, right

//create thumbnails for image1
$i = 0;
$WASengine->config['create_thumbnail']['image1'.$image_field_suffix]['prefixseperator'][$i] = "_"; 		//seperator that will be used between prefix and filename
$WASengine->config['create_thumbnail']['image1'.$image_field_suffix]['suffixseperator'][$i] = "";			//seperator that will be used between suffix and extension
$WASengine->config['create_thumbnail']['image1'.$image_field_suffix]['prefix'][$i]    = "th";    			//will be the prefix of the new filename, like; foobar.gif.thumb.gif
$WASengine->config['create_thumbnail']['image1'.$image_field_suffix]['suffix'][$i]    = "";    			//will be the suffix of the new filename, like; foobar.gif.thumb.gif
$WASengine->config['create_thumbnail']['image1'.$image_field_suffix]['width'][$i] = 450;        			//when empty, it will be calculated against the height
$WASengine->config['create_thumbnail']['image1'.$image_field_suffix]['height'][$i] = 9999;					//when empty, it will be calculated against the width
$WASengine->config['create_thumbnail']['image1'.$image_field_suffix]['resizemethod'][$i] = "keep_aspect";	//keep_aspect or fit, empty for no resize
$WASengine->config['create_thumbnail']['image1'.$image_field_suffix]['croptosize'][$i] = false;            	//if set to TRUE, the image is cropped to exacltly the specified size
$WASengine->config['create_thumbnail']['image1'.$image_field_suffix]['cropmethod'][$i] = "center";			//center, top, bottom, left, right


//PUT HERE YOUR MODULE CONFIG LINES!

$WASengine->config['current_table'] = "vereniging"; //main MySQL table for this module
$WASengine->config['filterSelectFields'] = ""; //Hard code fields to filter the listing on. Seperate with a comma. Example: "title,tableB.name,tableA.comment"
$WASengine->config['listing_HTML'] = "strip"; //HTML data in listing: ascii/execute/strip

$WASengine->config['FastSearch_tables']  = "";      //Set extra tables. Set only tables you used in your query!!


$WASengine->config['requiredFields']['naam'] = $WASengine->lang("Naam"); //set required

//the fieldname in the crosslink table from the current
$WASengine->config['crosslinktable']['baan_vereniging_clt']['currfield'] = 'id_vereniging';
//the fieldname in the crosslink table from the linked module
$WASengine->config['crosslinktable']['baan_vereniging_clt']['linkedfield'] = 'id_baan';


//END CONFIG

//DO SOMETHING WITH POSTED VARS
if($_SERVER['REQUEST_METHOD'] == 'POST') {


	//loop all checkboxes
	$checkboxArr = array('dames', 'heren');
	foreach($checkboxArr AS $cb) {
		if($WASengine->HTTP_POST_VARS[$cb] != '1') {
			$WASengine->HTTP_POST_VARS[$cb] = '0';
		}
	}

}

//do standard module stuff, like insert, delete, listing, etc.
$WASengine->processModule("SELECT
						vereniging.naam,
						vereniging.plaats
					FROM
						vereniging
					ORDER BY
						vereniging.id_vereniging");


//START THE FORM

//form header
$formBuilder->form['action'] = $WASengine->config['URL'];
$formBuilder->form['method'] = "POST";
$formBuilder->form['enctype'] = "multipart/form-data";




//hidden input
$formBuilder->field['id_vereniging']['type'] = "hidden";
$formBuilder->field['id_vereniging']['param']['value'] = $WASengine->recordRow['id_vereniging'];


//text input naam
$formBuilder->field['naam']['label'] = $WASengine->lang("Naam");
$formBuilder->field['naam']['type'] = "input";
$formBuilder->field['naam']['param']['type'] = "text";
$formBuilder->field['naam']['param']['value'] = htmlspecialchars($WASengine->recordRow['naam']);


//text input naam
$formBuilder->field['contact_naam']['label'] = $WASengine->lang("Contact Naam");
$formBuilder->field['contact_naam']['type'] = "input";
$formBuilder->field['contact_naam']['param']['type'] = "text";
$formBuilder->field['contact_naam']['param']['value'] = htmlspecialchars($WASengine->recordRow['contact_naam']);



//text input straat
$formBuilder->field['straat']['label'] = $WASengine->lang("Straat");
$formBuilder->field['straat']['type'] = "input";
$formBuilder->field['straat']['param']['type'] = "text";
$formBuilder->field['straat']['param']['value'] = $WASengine->recordRow['straat'];



//text input postcode
$formBuilder->field['postcode']['label'] = $WASengine->lang("Postcode");
$formBuilder->field['postcode']['type'] = "input";
$formBuilder->field['postcode']['param']['type'] = "text";
$formBuilder->field['postcode']['param']['value'] = $WASengine->recordRow['postcode'];



//text input huisnummer
$formBuilder->field['huisnummer']['label'] = $WASengine->lang("Huisnummer");
$formBuilder->field['huisnummer']['type'] = "input";
$formBuilder->field['huisnummer']['param']['type'] = "text";
$formBuilder->field['huisnummer']['param']['value'] = $WASengine->recordRow['huisnummer'];



//text input plaats
$formBuilder->field['plaats']['label'] = $WASengine->lang("Plaats");
$formBuilder->field['plaats']['type'] = "input";
$formBuilder->field['plaats']['param']['type'] = "text";
$formBuilder->field['plaats']['param']['value'] = $WASengine->recordRow['plaats'];



//integer input telefoon
$formBuilder->field['telefoon']['label'] = $WASengine->lang("Telefoon");
$formBuilder->field['telefoon']['type'] = "input";
$formBuilder->field['telefoon']['param']['type'] = "text";
$formBuilder->field['telefoon']['param']['size'] = "13";
$formBuilder->field['telefoon']['param']['maxlength'] = "11";
$formBuilder->field['telefoon']['param']['value'] = $WASengine->recordRow['telefoon'];


//text input naam
$formBuilder->field['email']['label'] = $WASengine->lang("E-mail");
$formBuilder->field['email']['type'] = "input";
$formBuilder->field['email']['param']['type'] = "text";
$formBuilder->field['email']['param']['value'] = htmlspecialchars($WASengine->recordRow['email']);

//text input naam
$formBuilder->field['clubavond']['label'] = $WASengine->lang("Clubavond");
$formBuilder->field['clubavond']['type'] = "input";
$formBuilder->field['clubavond']['param']['type'] = "text";
$formBuilder->field['clubavond']['param']['value'] = htmlspecialchars($WASengine->recordRow['clubavond']);

//integer input dames
$formBuilder->field['dames']['label'] = $WASengine->lang("Dames");
$formBuilder->field['dames']['type'] = "input";
$formBuilder->field['dames']['default'] = 0;
$formBuilder->field['dames']['param']['type'] = "checkbox";
$formBuilder->field['dames']['param']['value'] = 1;
if($WASengine->recordRow['dames']){
	$formBuilder->field['dames']['param']['checked'] = "";
}

//integer input heren
$formBuilder->field['heren']['label'] = $WASengine->lang("Heren");
$formBuilder->field['heren']['type'] = "input";
$formBuilder->field['heren']['default'] = 0;
$formBuilder->field['heren']['param']['type'] = "checkbox";
$formBuilder->field['heren']['param']['value'] = 1;
if($WASengine->recordRow['heren']){
	$formBuilder->field['heren']['param']['checked'] = "";
}

//text area opmerkingen
$formBuilder->field['opmerkingen']['label'] = $WASengine->lang("Opmerkingen");
$formBuilder->field['opmerkingen']['type'] = "textarea";
$formBuilder->field['opmerkingen']['param']['style'] = "width:450px;height:300px;";
//$formBuilder->field['opmerkingen']['param']['rows'] = "10";
$formBuilder->field['opmerkingen']['param']['wrap'] = "on";
$formBuilder->field['opmerkingen']['value'] = $WASengine->recordRow['opmerkingen'];
$formBuilder->field['opmerkingen']['suffix'] = "<script>editor_generate('opmerkingen',config);</script>";
//disable 'insert file' button...
$formBuilder->field['opmerkingen']['file_lib'] = false;



//file/image upload input image1
$formBuilder->field['image1']['label'] = $WASengine->lang("Image1");
$formBuilder->field['image1']['type'] = "input";
$formBuilder->field['image1']['param']['type'] = "file";
if($WASengine->recordRow['image1']) {
    $formBuilder->field['image1']['suffix'] = $WASengine->parseUploadedImage('image1');
}





//integer input id_persoon_voorzitter
$formBuilder->field['id_persoon_voorzitter']['label'] = $WASengine->lang("Id persoon voorzitter");
$formBuilder->field['id_persoon_voorzitter']['type'] = "input";
$formBuilder->field['id_persoon_voorzitter']['param']['type'] = "text";
$formBuilder->field['id_persoon_voorzitter']['param']['size'] = "13";
$formBuilder->field['id_persoon_voorzitter']['param']['maxlength'] = "11";
$formBuilder->field['id_persoon_voorzitter']['param']['value'] = $WASengine->recordRow['id_persoon_voorzitter'];


//integer input id_persoon_secretaris
$formBuilder->field['id_persoon_secretaris']['label'] = $WASengine->lang("Id persoon secretaris");
$formBuilder->field['id_persoon_secretaris']['type'] = "input";
$formBuilder->field['id_persoon_secretaris']['param']['type'] = "text";
$formBuilder->field['id_persoon_secretaris']['param']['size'] = "13";
$formBuilder->field['id_persoon_secretaris']['param']['maxlength'] = "11";
$formBuilder->field['id_persoon_secretaris']['param']['value'] = $WASengine->recordRow['id_persoon_secretaris'];


//integer input id_persoon_penningmeester
$formBuilder->field['id_persoon_penningmeester']['label'] = $WASengine->lang("Id persoon penningmeester");
$formBuilder->field['id_persoon_penningmeester']['type'] = "input";
$formBuilder->field['id_persoon_penningmeester']['param']['type'] = "text";
$formBuilder->field['id_persoon_penningmeester']['param']['size'] = "13";
$formBuilder->field['id_persoon_penningmeester']['param']['maxlength'] = "11";
$formBuilder->field['id_persoon_penningmeester']['param']['value'] = $WASengine->recordRow['id_persoon_penningmeester'];



//select activiteitID
$formBuilder->field['baan_vereniging_clt[]']['label'] = "Banen";
$formBuilder->field['baan_vereniging_clt[]']['type'] = "select";
$formBuilder->field['baan_vereniging_clt[]']['param']['size']      = "10";
$formBuilder->field['baan_vereniging_clt[]']['param']['multiple']    = "";
$WASengine->makeCrosslinkSelectOptions('vereniging','id_vereniging',$WASengine->recordRow['id_vereniging'],'baan','id_baan','naam','id_baan',"","","",'baan_vereniging_clt');



//END THIS MODULE

//parse the module
$WASengine->parseModule();

?>
