<?php


class Model{{ tablename|title }} extends Model {

	public function get{{ tablename|title }}Items() {
		$sql = "SELECT * FROM {{ tablename }}";
		return $this->db->getObjecten($sql);
	}

	public function get{{ tablename|title }}ItemsHome(){

		$sql = "SELECT * FROM {{ tablename }}";
		return $this->db->getObjecten($sql);

	}

	public function get{{ tablename|title }}Item($id){

		$id = (int) $id;
		$sql = "SELECT * FROM {{ tablename }} WHERE id_{{ tablename }}='{$id}'";
		return $this->db->getObject($sql);

	}

	public function slug($string){
	    return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
	}


	public function install(){
		return true;
	}

	//EOC
}
