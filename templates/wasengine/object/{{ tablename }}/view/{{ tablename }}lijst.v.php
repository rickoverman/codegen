<?php

class View{{ tablename|title }}Lijst extends View {


	public function index(){

                // Get the {{ tablename }} items from the database.
                $this->load->model('object', '{{ tablename }}');
                $this->data['{{ tablename }}'] = $this->model_{{ tablename }}->get{{ tablename|title }}Items();

        }


        public function detail(){

                $this->load->model('object', '{{ tablename }}');
                $this->data['{{ tablename }}'] = $this->model_{{ tablename }}->get{{ tablename|title }}Item();

        }


	//EOC

}
