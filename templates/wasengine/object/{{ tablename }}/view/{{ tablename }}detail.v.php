<?php

class View{{ tablename }}detail extends View {

    public function index(){

        $this->load->model('object', '{{ tablename }}');
        $url = $this->url->get('url_module_items');
        $id_baan  = isset($url[2])?(int)$url[2]:0;

        if($id_{{ tablename }} && $this->model_{{ tablename }}->get{{ tablename|title }}Item($id_baan)){
            $this->data['{{ tablename }}'] = $this->model_{{ tablename }}->get{{ tablename|title }}Item($id_baan);
        }
    }

}
