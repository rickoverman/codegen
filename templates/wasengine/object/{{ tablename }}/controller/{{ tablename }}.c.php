<?php

class Controller{{ tablename|title }} extends Controller{

	public function index(){

                $this->load->model('object', '{{ tablename }}');

                $url = $this->url->get('url_module_items');
                $id_{{ tablename }}  = isset($url[2])?(int)$url[2]:0;

                if($id_{{ tablename }} && $this->model_{{ tablename }}->get{{ tablename|title }}Item($id_{{ tablename }})){
                    $this->detail($id_{{ tablename }});
                }else{
                    $this->lijst();
                }

                return $this->render();

	}

        public function lijst() {

								//Initieer de view
								$this->load->view('object', '{{ tablename }}','{{ tablename }}lijst');

                //Defineer de template
                $this->template = 'object/{{ tablename }}/template/{{ tablename }}lijst.t.php';

                //Haal ui uit de view
                $this->data = $this->view_{{ tablename }}lijst->getData();

	}

        public function detail() {

						//Initieer de view
						$this->load->view('object', '{{ tablename }}','{{ tablename }}detail');

	          //Defineer de template
	          $this->template = 'object/{{ tablename }}/template/{{ tablename }}detail.t.php';

	          //Haal ui uit de view
	          $this->data = $this->view_{{ tablename }}detail->getData();

	}

			public function install(){
				$this->load->model('object', '{{ tablename }}');
				$this->model_{{ tablename }}->install();
				return true;
			}

	//EOC

}
